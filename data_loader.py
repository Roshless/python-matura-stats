import csv
import json
import os
import urllib.request
import requests

from database_handler import DatabaseHandler


class LoadData(object):
    db_handler = DatabaseHandler()
    csv_file_name = 'data.csv'
    api_url = 'https://api.dane.gov.pl/resources/17363'

    def __init__(self):
        if not self.db_handler.database_existed:
            self.download_csv()
            self.load_into_database()
            os.remove(self.csv_file_name)

    def load_into_database(self):
        with open(self.csv_file_name, newline='', encoding='cp1250') as csvfile:
            reader = csv.reader(csvfile, delimiter=';', quotechar='|')
            for id, row in enumerate(reader):
                # Do not add first line and whole country stats
                if id == 0 or 'Polska' in row:
                    continue
                if row[1] == 'przystąpiło':
                    del row[1]
                    self.db_handler.add_joined(row)
                elif row[1] == 'zdało':
                    del row[1]
                    self.db_handler.add_passed(row)

    def download_csv(self):
        with urllib.request.urlopen(self.api_url) as url:
            data = json.loads(url.read().decode())
            csv_file_url = data['data']['attributes']['link']

        r = requests.get(csv_file_url)

        with open(self.csv_file_name, 'wb') as f:
            f.write(r.content)
