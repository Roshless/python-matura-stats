import argparse
from builtins import int, str

from data_manipulations import DataManipulation
from data_loader import LoadData
from regions import regions

data = LoadData()


def parser_init():
    parser = argparse.ArgumentParser(prog='matura_stats', description='Simple tool for displaying stats about matura '
                                                                      'exams from 2010 to last year.')
    subparsers = parser.add_subparsers(help='Commands to chose from.', dest='command', required=True)

    average = subparsers.add_parser('average',
                                    help='Returns average number of people that joined the exam since 2010 to X year.')
    average.add_argument('region', type=str, help='Voivodeship', choices=regions)
    average.add_argument('year', type=int, help='To X year')

    percent_success = subparsers.add_parser('percent',
                                            help='Percentage of success in specified region over the years.')
    percent_success.add_argument('region', type=str, help='Voivodeship', choices=regions)

    highest_pass_rate = subparsers.add_parser('highest-pass-rate',
                                              help='Show which region had highest pass rate in year X.')
    highest_pass_rate.add_argument('year', type=int, help='Year')

    regressions = subparsers.add_parser('regressions',
                                        help='Show regions which regressed with years.')

    comparison = subparsers.add_parser('comparison',
                                       help='Compare pass rate each year between 2 regions.')
    comparison.add_argument('region', nargs=2, type=str, help='Voivodeship', choices=regions)

    group = parser.add_mutually_exclusive_group(required=False)
    group.add_argument('--men-only', dest='male', action='store_true', help='Show only men.')
    group.add_argument('--women-only', dest='female', action='store_true', help='Show only women.')
    return parser


def main():
    args = parser_init().parse_args()

    data = DataManipulation(args.male, args.female)

    if args.command == 'average':
        print(data.average(args.region, args.year))
    elif args.command == 'percent':
        print('\n'.join(data.percent(args.region)))
    elif args.command == 'highest-pass-rate':
        print(data.highest_pass_rate(args.year))
    elif args.command == 'regressions':
        print('\n'.join(data.regressions()))
    elif args.command == 'comparison':
        print('\n'.join(data.comparison(args.region)))


if __name__ == "__main__":
    main()
