import pytest

from data_loader import LoadData
from data_manipulations import DataManipulation

data = LoadData()

both = DataManipulation(False, False)
women = DataManipulation(False, True)
men = DataManipulation(True, False)


def test_average_1():
    assert both.average('Dolnośląskie', 2010) == 23399


def test_average_2():
    with pytest.raises(Exception):
        both.average('Dolnośląskieee', 2010)


def test_average_3():
    assert both.average('Pomorskie', 2010) != 12345


def test_average_4():
    assert men.average('Lubelskie', 2014) == pytest.approx(9536)


def test_percent1():
    assert both.percent(
        'Małopolskie') == ['2010 - 82.6%', '2011 - 77.3%', '2012 - 82.4%', '2013 - 83.5%', '2014 - 72.8%',
                           '2015 - 77.5%', '2016 - 82.1%', '2017 - 82.6%', '2018 - 83.2%']


def test_percent2():
    assert women.percent(
        'Opolskie') == ['2010 - 82.6%', '2011 - 75.7%', '2012 - 80.7%', '2013 - 82.2%', '2014 - 71.3%',
                        '2015 - 75.2%', '2016 - 78.6%', '2017 - 77.4%', '2018 - 78.6%']


def test_highest_pass_rate1():
    assert both.highest_pass_rate('2013') == '2013 - Małopolskie 83.5%'


def test_highest_pass_rate2():
    assert women.highest_pass_rate('2015') == '2015 - Małopolskie 77.5%'


# def test_regressions():
#    both.regressions()


def test_comparison():
    assert men.comparison(('Dolnośląskie', 'Pomorskie')) == ['2010 - Dolnośląskie', '2011 - Pomorskie',
                                                             '2012 - Dolnośląskie', '2013 - Pomorskie',
                                                             '2014 - Pomorskie', '2015 - Pomorskie', '2016 - Pomorskie',
                                                             '2017 - Pomorskie', '2018 - Dolnośląskie']
